# Merge trains demo

This is a sample project to demonstrate the use of merge trains.


If you happen to copy this project over you will need to ensure you have enabled merge trains on the project.

# Enabling merge trains for project

1. Visit Settings > General > Merge requests
2. Check `Enable merged results pipelines` and `Enable merge trains`

Run train change, morechnge